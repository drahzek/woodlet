package com.drahzek.woodlet;

import com.drahzek.woodlet.model.Tree;

public class Main {
    public static void main(String[] args) {


        TreeDAO dao = new TreeDAO();

        Tree tree1 = new Tree("Oak", 22);
        Tree tree2 = new Tree("Birch", 13);
        Tree tree3 = new Tree("Birch", 11);
        Tree tree4 = new Tree("Oak", 13);
        Tree tree5 = new Tree("Maple", 17);

        tree1 = dao.persist(tree1);
        tree2 = dao.persist(tree2);
        tree3 = dao.persist(tree3);
        tree4 = dao.persist(tree4);
        tree5 = dao.persist(tree5);

        dao.merge(tree1);
        dao.merge(tree2);
        dao.merge(tree3);
        dao.merge(tree4);
        dao.merge(tree5);

        dao.findAll().forEach(System.out::println);

        dao.delete(tree1);
        dao.delete(tree2);
        dao.delete(tree3);
        dao.delete(tree4);
        dao.delete(tree5);

        TreesEntityManagerFactory.close();
    }
}
