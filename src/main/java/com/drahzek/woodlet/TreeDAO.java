package com.drahzek.woodlet;

import com.drahzek.woodlet.model.Tree;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class TreeDAO {

    public Tree persist(Tree tree) {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(tree);
        tx.commit();
        em.close();
        return tree;
    }

    public Tree merge(Tree tree) {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        tree = em.merge(tree);
        tx.commit();
        em.close();
        return tree;
    }

    public Tree find(int id) {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        Tree tree = em.find(Tree.class, id);
        em.close();
        return tree;
    }

    public void delete(Tree tree) {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(tree));//Removing must be done in transaction.
        tx.commit();
        em.close();
    }

    public List<Tree> findAll() {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();

        CriteriaBuilder cb = em.getCriteriaBuilder();//Builder for creating a query.
        CriteriaQuery<Tree> query = cb.createQuery(Tree.class);//Query object declaring query return object.
        query.from(Tree.class);//From which entity (table) data will be fetched.
        List<Tree> trees = em.createQuery(query).getResultList();
        em.close();
        return trees;
    }
}
