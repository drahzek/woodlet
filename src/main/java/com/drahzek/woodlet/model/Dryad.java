package com.drahzek.woodlet.model;


import javax.persistence.*;

@Entity
@Table(name = "dryads")
public class Dryad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;
    @Column
    private int power;

    @ManyToOne
    @JoinColumn(name = "tree", referencedColumnName = "id")
    private Tree tree;

    public Dryad () {
    }

    public Dryad(String name, int power, Tree tree) {
        this.name = name;
        this.power = power;
        this.tree = tree;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }

    @Override
    public String toString() {
        return "Dryad{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", power=" + power +
                ", tree=" + tree +
                '}';
    }
}
