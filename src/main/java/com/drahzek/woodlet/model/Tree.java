package com.drahzek.woodlet.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "trees")
public class Tree {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String species;
    @Column
    private int height;

    public Tree() {
    }

    public Tree(String species, int height) {
        this.species = species;
        this.height = height;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Tree{" +
                "id=" + id +
                ", species='" + species + '\'' +
                ", height=" + height +
                '}';
    }
}
